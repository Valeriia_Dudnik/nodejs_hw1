const fs = require('fs');
const express = require('express');
const morgan = require('morgan')
const app = express();
const port = process.env.port ?? 8080;

const { filesRouter } = require('./filesRouter.js');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/files', filesRouter);

const start = async () => {
  try {
    if (!fs.existsSync('files')) {
      fs.mkdirSync('files');
    }
    app.listen(port);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
}

start();

app.get('/', (req, res) => {
  res.send('<h1>Server is running...</h1>')
})

app.use(errorHandler)

function errorHandler (err, req, res, next) {
  console.error('err')
  res.status(500).send({'message': 'Server error'});
}
