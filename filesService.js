const fs = require('fs');
const path = require('path');

const allowedExtensions = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];

function createFile (req, res, next) {
  const { filename, content } = req.body;
  
  if (!content) {
    res.status(400).send({ "message": "Please specify 'content' parameter" });

  } else if (!filename) {
    res.status(400).send({ "message": "Please specify 'filename' parameter" });

  } else if (!allowedExtensions.includes(path.extname(filename))) {
    res.status(400).send({ "message": "Please try another file extension" });

  } else if (filename && content) {
    fs.writeFileSync(`./files/${filename}`, content );
    res.status(200).send({ "message": "File created successfully" });
  }
}

function getFiles (req, res, next) {
  try { 
  const filenames = fs.readdirSync('./files');
    res.status(200).send({"message": "Success", "files": filenames});

  } catch (error){
    res.status(400).send({"message": "Client error"});
  }
}

function getFile (req, res, next) {
    const filename = req.params.filename;
    const filenames = fs.readdirSync('./files');

      if (filenames.includes(filename)) {
        
        const content = fs.readFileSync(`./files/${filename}`, 'utf8');
        const date = fs.statSync(`./files/${filename}`).birthtime;
        const extention = path.extname(filename).slice(1);

        res.status(200).send({
        "message": "Success",
        "filename": filename,
        "content": content,
        "extension": extention,
        "uploadedDate": date});

      } else {
        res.status(400).send({"message": `No file with ${filename} filename found`});
    }
}

function editFile (req, res, next) {
  const oldFilename = req.params.filename;
  const {filename, content} = req.body;
  const filenames = fs.readdirSync('./files');
  const rightExtention = allowedExtensions.includes(path.extname(filename));

  if (filenames.includes(oldFilename)) {

    if (!filename) {
      res.status(400).send({ "message": "Please specify 'filename' parameter" });

    } else if (!content) {
      res.status(400).send({ "message": "Please specify 'content' parameter" });

    } else if (!rightExtention) {
      res.status(400).send({ "message": "Please try another file extension" });

    } else if (filename && content) {
      fs.writeFileSync(`./files/${filename}`, content);
      fs.renameSync(`./files/${oldFilename}`, `./files/${filename}`);
      res.status(200).send({"message": `File ${oldFilename} changed to ${filename} successfully`});
    }

  } else {
    res.status(400).send({"message": `No file with ${oldFilename} filename found`});
  }
}

function deleteFile (req, res, next) {
  const filename = req.params.filename;
  const filenames = fs.readdirSync('./files');

  if (filenames.includes(filename)) {
    fs.unlinkSync(`./files/${filename}`)
    res.status(200).send({"message": "File deleted successfully"});
  
  } else {
    res.status(400).send({"message": `No file with ${filename} filename found`});
  }
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile
}
