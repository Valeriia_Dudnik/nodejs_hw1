const express = require('express');
const router = express.Router();
const { createFile, getFiles, getFile, editFile, deleteFile } = require('./filesService.js');

router.post('/', createFile);

router.get('/', getFiles); 

router
  .route('/:filename')
  .get(getFile)
  .put(editFile)
  .delete(deleteFile);

module.exports = {
  filesRouter: router
};
